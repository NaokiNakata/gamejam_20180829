﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drop : MonoBehaviour {
	private Rigidbody _rigid = null;
	private string _tag = null;
	public Vector2Int _coord = new Vector2Int(-1, -1);
	private DropsManager _dm = null;

	// Use this for initialization
	void Start () {
		_rigid = this.GetComponent<Rigidbody>();
		if(_rigid == null){
			Debug.Log("Can't find rigid body");
		}

		_tag = this.gameObject.transform.tag;
		if(_tag == null){
			Debug.Log("Can't find tag");
		}

		GameObject dm = GameObject.Find("DropsManager");
		_dm = dm.GetComponent<DropsManager>();
		if(_dm == null){
			Debug.Log("Can't find drops manager");
		}
	}
	
	// Update is called once per frame
	void Update () {
		double speed = _rigid.velocity.magnitude;

		// 運動が止まっている場合、位置情報を計算する
		Vector2Int newCoord = new Vector2Int(-1, -1);
		if(speed < 0.0001){
			Vector3 pos = this.gameObject.transform.position;
			//newCoord = CalcCoordinate(pos.x, pos.y);
			newCoord = _dm.ConvertPos2Cood(new Vector2(pos.x, pos.y));
		}

		// 位置情報が変化した場合更新
		if(_coord != newCoord){
			UpdateCoordinate(newCoord);
			_dm.UpdateDropStatus(_coord, _tag);
		}
	}

	void UpdateCoordinate (Vector2Int newCoordinate) {
		this._coord = newCoordinate;
		//Debug.Log(_coord);
	}

	public void Destroy (){
		_dm.AddPoint(1);
		Destroy(this.gameObject);
	}
}
