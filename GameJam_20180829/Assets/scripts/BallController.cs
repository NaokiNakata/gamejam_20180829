﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BallController : MonoBehaviour , IBeginDragHandler, IDragHandler, IEndDragHandler {
	private Rigidbody _rigid = null;
	private DropsManager _dm = null;

	Vector2 startPosition; // タッチし始めの座標

	bool isMoving = false;

	// Use this for initialization
	void Start () {
		_rigid = this.GetComponent<Rigidbody>();
		if(_rigid == null){
			Debug.Log("Can't find rigid body");
		}

		GameObject dm = GameObject.Find("DropsManager");
		_dm = dm.GetComponent<DropsManager>();
		if(_dm == null){
			Debug.Log("Can't find drops manager");
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnBeginDrag(PointerEventData data) {
		Debug.Log("begin drag");
		Debug.Log(data.position);
		startPosition = data.position;
	}

    public void OnDrag(PointerEventData data) {
		Debug.Log("drag");
		var diff = data.position - startPosition;
		Debug.Log(diff);
	}

	public void OnEndDrag(PointerEventData data) {
		var diff = data.position - startPosition;
		Debug.Log("go!!");
		var magnitudeLimit = 50f;
		Debug.Log(diff.magnitude);
		var magnitudeLimitRatio = magnitudeLimit / Mathf.Max(diff.magnitude, magnitudeLimit);
		Move(-diff * magnitudeLimitRatio * 0.5f);

		Physics.gravity = new Vector3(0, -0.3f, 0);
	}

	public void Move(Vector2 velocity) {
		_rigid.velocity = velocity;
		isMoving = true;

		StartCoroutine("Destroy");
	}

	void OnTriggerEnter(Collider other) {
		GameObject colObj = other.gameObject;
		Rigidbody rig = GetComponent<Rigidbody>();

		// バンパーの場合は反射
		if(colObj.tag == "SideBumper"){
			Debug.Log("Side Reflect");
			rig.velocity= new Vector3(-rig.velocity.x, rig.velocity.y, 0);
			
		}else if(colObj.tag == "TopBumper"){
			Debug.Log("Top Reflect");	
			rig.velocity= new Vector3(rig.velocity.x, -rig.velocity.y, 0);

		// ドロップの場合は破壊
		}else if(colObj.tag.Contains("Drop")){
			Drop drop = other.gameObject.GetComponent<Drop>();
			drop.Destroy();

			// 減衰処理
			float damp = 0.9f;
			rig.velocity = new Vector3(rig.velocity.x * damp, rig.velocity.y * damp, 0);
		}
	}

	IEnumerator Destroy(){
		yield return new WaitForSeconds (1.5f);
		Physics.gravity = new Vector3(0, -9.81f, 0);

		Destroy(this.gameObject);  

		// これ以下が実行されない
		yield return new WaitForSeconds (2.0f);
		_dm.ChainDrops();
	}
}
