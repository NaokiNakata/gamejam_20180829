﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {
	public Text score1;
	public Text score2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void UpdateScore(int[] score){
		score1.text = "Player1: " + score[0];
		score2.text = "Player2: " + score[1];
	}
}
