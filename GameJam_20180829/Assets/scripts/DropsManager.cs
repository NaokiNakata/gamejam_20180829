﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropsManager : MonoBehaviour {

	// 停止しているドロップのカウンター
	private int _validDropCounter = 0;
	// 各座標にあるドロップの情報
	private string[,] _dropsMap = new string[7, 7];
	public GameObject[] dropPrefabs;
	private GameManager _gm;

	private int _point = 0;

	// Use this for initialization
	IEnumerator Start () {
		GameObject gm = GameObject.Find("GameManager");
		_gm = gm.GetComponent<GameManager>();
		if(_gm == null){
			Debug.Log("Can't find game manager");
		}

		// マップ初期化
		InitDropsMap();

    	yield return new WaitForSeconds (1.0f);

		// ドロップ生成
		InitDrops();

	    yield return new WaitForSeconds (2.0f);

		yield return ChainDrops();
	}
	
	// Update is called once per frame
	void Update () {

	}

	// ドロップの初期配置生成
	void InitDrops(){
		int[] arrCoodX = {1, 2, 3, 4, 5, 6};
		int[] arrCoodY = {1, 2, 3, 4, 5, 6};
		foreach( int coodX in arrCoodX){
			foreach(int coodY in arrCoodY){
				GenerateRandomDrop(ConvertCood2Pos(new Vector2Int(coodX, coodY)));
			}
		}
	}

	// ドロップマップの情報の初期化
	void InitDropsMap(){
		for(int x = 1; x <= 6; x++){
			for(int y = 1; y <= 6; y++){
				_dropsMap[x, y]  = "";
			}
		}
	}

	// 与えられた位置にランダムなドロップを生成する
	void GenerateRandomDrop(Vector2 pos){
		// 生成するドロップをランダムで選択
		int index = (int)Random.Range(0, dropPrefabs.Length);

		Instantiate(dropPrefabs[index], 
					new Vector3(pos.x, pos.y, 0), 
					Quaternion.identity);
	}

	// 消去されたドロップの補充を行う
	// 引数 int[7] : 各列に補充するドロップの個数(index=0は使わない)
	void FillDrops(int[] fillCounter){
		//Debug.Log("fill: ");
		for(int x = 1; x <= 6; x++){
			for(int y = 1; y <= fillCounter[x]; y++){
				Vector2 pos = ConvertCood2Pos(new Vector2Int(x, y));

				// コンバート後にy+=7.0f
				GenerateRandomDrop(new Vector2(pos.x, pos.y+7.0f));
			}
		}
	}

	// 連鎖によるドロップの消去を行う
	public IEnumerator ChainDrops(){
		Queue<Queue<Vector2Int>> chainQueue = new Queue<Queue<Vector2Int>>();

		// 全ドロップを走査し、連鎖の組([[cood,cood...], [cood, cood...], ...])を取得
		bool[,] isChained = new bool[7, 7];

		for(int x = 1; x <= 6; x++){
			for(int y = 1; y <= 6; y++){
				if(!isChained[x, y]){
					// 連鎖するか走査
					Queue<Vector2Int> chain = SearchChain(new Vector2Int(x, y));
					//Debug.Log("search:"+x+","+y);

					// 連鎖する場合
					if(chain != null){
						chainQueue.Enqueue(chain);
						foreach(Vector2Int cood in chain){
							// マップを更新
							isChained[cood.x, cood.y] = true;
						}
					}
				}
			}
		}

		// //Debug.Log("print chain");
		// foreach(Queue<Vector2Int> chian in chainQueue){
		// 	string s = "[";
		// 	foreach(Vector2Int cood in chian){
		// 		s = s + cood + ",";
		// 	}
		// 	s+="]";
		// 	Debug.Log(s);
		// }

		// 連鎖がある場合はドロップを消去
		int[] fillLane = new int[7];
		int deleteDropsCount = 0;
		Physics.gravity = new Vector3(0, -0.3f, 0);
		foreach(Queue<Vector2Int> chain in chainQueue){
			// 消えるドロップのレーン情報を取得
			int[] lane = GetDropsLane(chain);
			fillLane = AddIntArray(fillLane, lane);

			deleteDropsCount += DeleteChain(chain);
			yield return new WaitForSeconds (1f);
		}
		Physics.gravity = new Vector3(0, -9.81f, 0);

		// ドロップが消去された場合はドロップを補充
		if(deleteDropsCount > 0){
			AddPoint(deleteDropsCount);
			
			yield return new WaitForSeconds (1.0f);
			FillDrops(fillLane);

			// 補充後に再度連鎖チェック
			yield return new WaitForSeconds (3.0f);
			yield return ChainDrops();
		}else{
			_gm.AddScore(_point);
			_point = 0;
		}
	}

	// 引数の位置からの連鎖を探す
	Queue<Vector2Int> SearchChain(Vector2Int startCood){
		string searchTag = _dropsMap[startCood.x, startCood.y];

		Queue<Vector2Int> resultQueue = new Queue<Vector2Int>();
		resultQueue.Enqueue(startCood);

		// 探索用のキュー
		Queue<Vector2Int> searchQueue = new Queue<Vector2Int>();
		searchQueue.Enqueue(startCood);

		while(searchQueue.Count != 0){
			Vector2Int searchCood = searchQueue.Dequeue();

			int[] dx = {1, 0, -1, 0};
			int[] dy = {0, 1, 0, -1};
			for(int i = 0; i < 4; i++){
				int x = searchCood.x + dx[i];
				int y = searchCood.y + dy[i];

				// 有効な座標なら探索
				if(1 <= x && x <= 6 && 1 <= y && y <= 6){
					// タグが一致した場合はsearchQueueに追加
					if(_dropsMap[x, y] == searchTag){
						Vector2Int newCood = new Vector2Int(x, y);

						// 未追加なら追加する
						if(!resultQueue.Contains(newCood)){
							resultQueue.Enqueue(newCood);
							searchQueue.Enqueue(newCood);
						}
					}
				}
			}
		}
		
		// 連鎖が4以上ならそれを返す
		if(resultQueue.Count >= 4){
			return resultQueue;
		}else{
			return null;
		}
	}

	// 連鎖したドロップの消去
	int DeleteChain(Queue<Vector2Int> chain){
		int dropsCount = 0;

		// 半径の範囲内にあるオブジェクトを消す
		foreach(Vector2Int cood in chain){
			Vector2 pos = ConvertCood2Pos(cood);
			//Debug.Log("Delete: " + pos);
			Collider[] targets = Physics.OverlapSphere(new Vector3(pos.x, pos.y, 0), 0.3f);
			foreach(Collider col in targets){
				//GameObject obj = col.transform.parent.gameObject;
				GameObject obj = col.gameObject;
				if(obj.transform.tag != "Field" && obj.transform.tag != "Player"){
					Destroy(obj);
					dropsCount++;
				}
			}
		}

		return dropsCount;
	}

	// 連鎖に含まれる、各レーンのドロップ数を取得する
	int[] GetDropsLane(Queue<Vector2Int> chain){
		int[] lane = new int[7];
		foreach(Vector2Int cood in chain){
			lane[cood.x]++;
		}
		return lane;
	}

	// Dropの位置情報が変化した時に呼び出される
	public void UpdateDropStatus(Vector2Int cood, string tag){
		// 有効な位置情報の場合更新処理
		if(cood.x > 0 && cood.y > 0){
			if(_dropsMap[cood.x, cood.y] != tag){
				_dropsMap[cood.x, cood.y] = tag;
				_validDropCounter++;
			}
		}
	}

	public void AddPoint(int point){
		_point += point;
	}

	// ドロップの位置情報から座標への変換を行う
	public Vector2 ConvertCood2Pos(Vector2Int cood){
		Vector2 pos = new Vector2(-10f, -10f);

		// xの計算
		if(cood.x == 1){
			pos.x = -2.5f;
		}else if(cood.x == 2){
			pos.x = -1.5f;
		}else if(cood.x == 3){
			pos.x = -0.5f;
		}else if(cood.x == 4){
			pos.x = 0.5f;
		}else if(cood.x == 5){
			pos.x = 1.5f;
		}else if(cood.x == 6){
			pos.x = 2.5f;
		}

		// yの計算
		if(cood.y == 1){
			pos.y = -1f;
		}else if(cood.y == 2){
			pos.y = 0f;
		}else if(cood.y == 3){
			pos.y = 1f;
		}else if(cood.y == 4){
			pos.y = 2f;
		}else if(cood.y == 5){
			pos.y = 3f;
		}else if(cood.y == 6){
			pos.y = 4f;
		}

		return pos;
	} 

	// 座標からドロップの位置情報への変換を行う
	public Vector2Int ConvertPos2Cood(Vector2 pos){
		Vector2Int cood = new Vector2Int(-1, -1);

		// xの計算
		double x = pos.x;
		// lane1
		if(-3 < x && x <= -2){
			cood.x = 1;

		// lane2
		}else if(-2 < x && x <= -1){
			cood.x = 2; 

		// lane3
		}else if(-1 < x && x <= 0){
			cood.x = 3;

		// lane4
		}else if(0 < x && x <= 1){
			cood.x = 4;

		// lane5
		}else if(1 < x && x <= 2){
			cood.x = 5;

		// lane6
		}else if(2 < x && x <= 3){
			cood.x = 6;
		}


		// yの計算
		double y = pos.y;
		// lane1
		if(-1.5 < y && y <= -0.5){
			cood.y = 1;

		// lane2
		}else if(-0.5 < y && y <= 0.5){
			cood.y = 2; 

		// lane3
		}else if(0.5 < y && y <= 1.5){
			cood.y = 3;

		// lane4
		}else if(1.5 < y && y <= 2.5){
			cood.y = 4;

		// lane5
		}else if(2.5 < y && y <= 3.5){
			cood.y = 5;

		// lane6
		}else if(3.5 < y && y <= 4.5){
			cood.y = 6;
		}

		return cood;
	}

	int[] AddIntArray(int[] arr1, int[] arr2){
		int l1 = arr1.Length;
		int l2 = arr2.Length;

		int[] res;
		if(l1 >= l2){
			res = arr1;
			for(int i = 0; i < l2; i++){
				res[i] += arr2[i];
			}
		}else{
			res = arr2;
			for(int i = 0; i < l1; i++){
				res[i] += arr1[i];
			}
		}

		return res;
	}
}
