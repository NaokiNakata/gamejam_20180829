﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
	private int _playerNum = 2;
	private int _nowPlaying = 1;
	private int[] score = new int[2];

	private UIController _uiController = null;

	// Use this for initialization
	void Start () {
		score[0] = 0;
		score[1] = 0;

		GameObject uic = GameObject.Find("UIController");
		_uiController= uic.GetComponent<UIController>();
		if(_uiController == null){
			Debug.Log("Can't find UI Controller");
		}
		_uiController.UpdateScore(score);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void AddScore(int point){
		score[_nowPlaying-1] = score[_nowPlaying-1] + point;
		_uiController.UpdateScore(score);

		// プレイヤー切り替え
		if(_nowPlaying == 1){
			_nowPlaying = 2;
		}else{
			_nowPlaying = 1;
		}
	}
}
